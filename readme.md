## Informations générales

Adresse du site : https://maceofreguis.000webhostapp.com
J'ai testé le site principalement avec Firefox avec un écran full HD les autres navigateurs et écrans plus petits pourraient donc moins bien marcher.
Pour voir la provenance des images présentent dans le corps des pages, il faut passer la souris dessus. L'image de retour à l'accueil provient de Spotify et celles de la barre de navigation proviennent du site de la Fnac. 

## Tournée

Cette page contient :
- Un formulaire PHP, n'importe email et mot de passe peuvent être rentrés dans le formulaire tant que l'email a un format valide. Le formulaire permet "réserver" les places, quand le formulaire est validé, l'adresse email du compte doit apparaître à droite de la barre de navigation et un popup contenant l'adresse email du compte doit annoncer que le payement a été effectué.
- Deux calculs javascript, l'un calculant le total du prix de toutes les places réservées (nbplace * 90) et l'autre calculant le nombre total de places réservées.
- Une liste

## Civilisation, La fête est finie et le chant des sirènes

Ces pages contiennent :
- un tableau
- des intégrations avec Spotify permettant d'écouter des extraits des titres
- des liens vers des sites externes
- deux photos illustrant le sujet

## CSS

- Le fichier `global.css` est appliqué sur toutes les pages.
- Le fichier `tournee.css` est appliqué sur la page `tournee.php`
- Le fichier `albums.css` est appliqué aux pages `civilisation.html`, `lafêteestfinie.html` et `lechantdessirènes.html` 

Sur la page `index.html`, le bouton de retour à l'accueil fait remonter au début de la page.


Le design et l'architecture du site s'inspirent des sites [orelsan.show](https://orelsan.show/) et [orelsan7th.com](https://orelsan7th.com/), la palette de couleur reprend celle de l'album *Civilisation*.